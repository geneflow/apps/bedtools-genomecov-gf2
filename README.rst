Bedtools Genome Coverage GeneFlow App
=====================================

Version: 2.29.2-01

This GeneFlow app wraps the Bedtools genomecov tool.

Inputs
------

1. input: Input BAM File.

2. reference: Reference FASTA file.

Parameters
----------

1. output: Output coverage file.
